
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BookServicePage } from './../book-service/book-service';
import { MessagesPage } from '../messages/messages';
import { GoPaperlessPage } from './../go-paperless/go-paperless';
import { FacebookConnectPage } from './../facebook-connect/facebook-connect';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  tabOne:any;
  tabTwo:any;
  tabThree:any;
  tabFour:any;

  constructor(public navCtrl: NavController) {
    this.tabOne = BookServicePage;
    this.tabTwo = MessagesPage;
    this.tabThree = FacebookConnectPage;
    this.tabFour = GoPaperlessPage;
  }

}
