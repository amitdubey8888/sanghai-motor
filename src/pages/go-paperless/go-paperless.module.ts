import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoPaperlessPage } from './go-paperless';

@NgModule({
  declarations: [
    GoPaperlessPage,
  ],
  imports: [
    IonicPageModule.forChild(GoPaperlessPage),
  ],
})
export class GoPaperlessPageModule {}
