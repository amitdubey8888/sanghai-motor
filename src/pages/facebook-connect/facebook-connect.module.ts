import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacebookConnectPage } from './facebook-connect';

@NgModule({
  declarations: [
    FacebookConnectPage,
  ],
  imports: [
    IonicPageModule.forChild(FacebookConnectPage),
  ],
})
export class FacebookConnectPageModule {}
