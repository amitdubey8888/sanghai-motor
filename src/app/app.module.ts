import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { BookServicePage } from '../pages/book-service/book-service';
import { MessagesPage } from '../pages/messages/messages';
import { GoPaperlessPage } from '../pages/go-paperless/go-paperless';
import { FacebookConnectPage } from '../pages/facebook-connect/facebook-connect';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    BookServicePage,
    MessagesPage,
    GoPaperlessPage,
    FacebookConnectPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    BookServicePage,
    MessagesPage,
    GoPaperlessPage,
    FacebookConnectPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
