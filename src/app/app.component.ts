import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { BookServicePage } from '../pages/book-service/book-service';
import { MessagesPage } from '../pages/messages/messages';
import { GoPaperlessPage } from '../pages/go-paperless/go-paperless';
import { FacebookConnectPage } from '../pages/facebook-connect/facebook-connect';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage },
      { title: 'Book Service', component: BookServicePage },
      { title: 'Message', component: MessagesPage },
      { title: 'Go Paperless', component: GoPaperlessPage },
      { title: 'Facebook Connect', component: FacebookConnectPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
